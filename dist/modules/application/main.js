"use strict";

var configs = require("../../configs/application");

module.exports = function (app) {
  return configs[app];
};