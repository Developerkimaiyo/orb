"use strict";

/**
 * These functions are useful when a user is confirming 
 * an order that already has price requisition done
 */
module.exports = {
  pair: function pair(rider, tier) {
    if (!rider) {
      return {
        status: false,
        message: "Rider is not registered on the Sendy platform. Try again with a different phone number"
      };
    } else if (rider.rider_stat == 2) {
      return {
        status: false,
        message: "Rider is not allowed to service orders. Try again with a different rider"
      };
    } else if (rider.vendor_type != tier.vendor_id) {
      return {
        status: false,
        message: "The vehicle for the rider is not allowed to service this order. Try again with a different rider"
      };
    } else {
      return {
        status: true,
        details: {
          sim_card_sn: rider.phone_no_1,
          rider_phone: rider.phone_no
        }
      };
    }
  }
};