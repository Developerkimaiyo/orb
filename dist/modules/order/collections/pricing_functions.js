"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var api = require("../../api/main");
/**
 * These functions are useful when a user is placing a price request
 */


module.exports = {
  viewport: function () {
    var _viewport = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(_ref) {
      var locations, googleKey;
      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              locations = _ref.locations, googleKey = _ref.googleKey;
              return _context2.abrupt("return", Promise.all(locations.map( /*#__PURE__*/function () {
                var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(point) {
                  var googleUrl, coordinates, _yield$api$send, results;

                  return _regenerator["default"].wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          googleUrl = "https://maps.googleapis.com/maps/api/geocode/json";
                          coordinates = point.lat + "," + point["long"];
                          _context.next = 4;
                          return api.send({
                            "method": "get",
                            "app": googleUrl,
                            "params": {
                              "key": googleKey,
                              "fields": "geometry",
                              "latlng": coordinates
                            }
                          });

                        case 4:
                          _yield$api$send = _context.sent;
                          results = _yield$api$send.results;
                          return _context.abrupt("return", {
                            coordinates: coordinates,
                            type: point.type,
                            name: point.name,
                            more: {
                              viewport: results.length == 0 ? results : results[0].geometry.viewport
                            }
                          });

                        case 7:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x2) {
                  return _ref2.apply(this, arguments);
                };
              }())));

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function viewport(_x) {
      return _viewport.apply(this, arguments);
    }

    return viewport;
  }()
};