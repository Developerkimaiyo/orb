"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var orderCompletionFunctions = require("./collections/order_completion_functions");

var pricing_functions = require("./collections/pricing_functions");

module.exports = _objectSpread(_objectSpread(_objectSpread({}, orderCompletionFunctions), pricing_functions), {}, {
  /** 
   * Returns the order status given an order
   */
  status: function status(order) {
    var status = Number(order.status);
    var orderStatus = Number(order.order_status);
    var confirmStatus = Number(order.confirm_status);
    var deliveryStatus = Number(order.delivery_status);

    if (status === 1) {
      if (orderStatus === 0 && confirmStatus === 0 && deliveryStatus === 0) {
        return 'price request';
      }

      if (orderStatus === 1 && confirmStatus === 0 && deliveryStatus === 0) {
        return 'pending';
      }

      if (orderStatus === 1 && confirmStatus === 1 && deliveryStatus === 0) {
        return 'confirmed';
      }

      if (orderStatus === 1 && confirmStatus === 1 && deliveryStatus === 2) {
        return 'in transit';
      }

      if (orderStatus === 1 && confirmStatus === 1 && deliveryStatus === 3) {
        return 'delivered';
      }
    }

    return 'cancelled';
  },

  /**
   * Checks if an order can be cancelled.
   * @param { object } order
   * @return { object }
   */
  cancellable: function cancellable(order) {
    var api = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'private';
    var orderStatus = this.status(order);
    var response = {
      status: false,
      message: null
    };

    if (orderStatus == "pending" || orderStatus == "confirmed") {
      response.status = true;
    } else if (orderStatus == "cancelled") {
      response.message = "Order already cancelled";
    } else if (orderStatus == "in transit") {
      var isPrivateApi = api == 'private';
      response.status = isPrivateApi;
      response.message = isPrivateApi ? null : "Delivery in progress. Contact customer care to cancel.";
    } else {
      response.message = "Could not cancel order. Contact customer care for more details";
    }

    return response;
  }
});