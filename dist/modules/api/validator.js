"use strict";

var configs = require("../../configs/api");

var helpers = require("./helpers");
/** 
 * Ensures the app to which comms is being sent to is valid
 */


function appValidator(app) {
  if (!app) {
    throw new Error("Please provide an app name or a base url");
  }

  if (!helpers.isValidHttpUrl(app)) {
    var configuredApps = Object.keys(configs.apps);

    if (!configuredApps.includes(app)) {
      throw new Error(JSON.stringify({
        configured_apps: configuredApps,
        message: "The app '".concat(app, "' is not configured yet.")
      }));
    }
  }
}
/** 
 * Checks that the request is being sent to a supported HTTP method 
 * - "GET", "POST", "PATCH", "UPDATE", "DELETE"
 */


function methodValidator(method) {
  if (!method) {
    throw new Error("The http method is required");
  }

  if (!configs.http_methods.includes(method.toUpperCase())) {
    throw new Error(JSON.stringify({
      supported_methods: configs.method,
      message: "The http method '".concat(method, "' is not supported")
    }));
  }
}
/**
 * @hint - No need to validate endpoint and payload types since 
 * javascript http client libraries will mostly have these
 */


exports.validate = function (_ref) {
  var app = _ref.app,
      method = _ref.method,
      endpoint = _ref.endpoint;
  appValidator(app);
  methodValidator(method);
};