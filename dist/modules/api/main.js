"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var axios = require('axios');

var helpers = require("./helpers");

var validator = require("./validator");

module.exports = {
  /**
  * Sends a communication request to any one of our internal APIs, Apps or services
  * 
  * @param { string } method
  * @param { string } endpoint
  * @param { object } config
  * 
  * @return { promise }
  */
  send: function () {
    var _send = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(configs) {
      var _Object$assign, app, endpoint, request;

      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              validator.validate(configs);
              _Object$assign = Object.assign(configs, {
                url: helpers.getUrl(configs)
              }), app = _Object$assign.app, endpoint = _Object$assign.endpoint, request = (0, _objectWithoutProperties2["default"])(_Object$assign, ["app", "endpoint"]);
              return _context2.abrupt("return", new Promise( /*#__PURE__*/function () {
                var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(resolve, reject) {
                  var response;
                  return _regenerator["default"].wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.prev = 0;
                          _context.next = 3;
                          return axios(request);

                        case 3:
                          response = _context.sent;
                          resolve(response.data);
                          _context.next = 10;
                          break;

                        case 7:
                          _context.prev = 7;
                          _context.t0 = _context["catch"](0);
                          reject({
                            error: _context.t0,
                            request: request
                          });

                        case 10:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, null, [[0, 7]]);
                }));

                return function (_x2, _x3) {
                  return _ref.apply(this, arguments);
                };
              }()));

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function send(_x) {
      return _send.apply(this, arguments);
    }

    return send;
  }()
};