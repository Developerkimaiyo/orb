"use strict";

var _require = require("../../configs/api"),
    apps = _require.apps,
    auth = _require.auth;

var customConfigs = require("../../configs/orb");
/**
 * Adds auth key (SRE)
 * @param {*} params 
 * @param {*} url 
 */


function addAuthKey(params) {
  params = params || {};

  if (!params.authkey) {
    params['authkey'] = customConfigs.authKey;
  }

  return params;
}
/**
 * Adds query parameters
 * @param {*} params 
 * @param {*} url 
 */


function addQueryParams(params, url) {
  params = addAuthKey(params);
  var queryKeys = Object.keys(params);
  url = !url.includes("?") ? "".concat(url, "?") : url;
  queryKeys.forEach(function (key, index) {
    var value = params[key];

    if (Array.isArray(value)) {
      value.forEach(function (item, index) {
        url = index < value.length - 1 ? "".concat(url).concat(key, "[]=").concat(item) : "".concat(url, "&").concat(key, "[]=").concat(item);
      });
    } else {
      url = "".concat(url).concat(key, "=").concat(value);
    }

    if (index < queryKeys.length - 1) {
      url = "".concat(url, "&");
    }
  });
  return url;
}

module.exports = {
  trim: function trim(string) {
    return string.replace(/^\/|\/$/g, '');
  },
  environment: function environment(env) {
    return env || customConfigs.environment || 'staging';
  },

  /**
   * Checks if a string is a valid http url 
   */
  isValidHttpUrl: function isValidHttpUrl(string) {
    var url;

    try {
      url = new URL(string);
    } catch (_) {
      return false;
    }

    return url.protocol === "http:" || url.protocol === "https:";
  },

  /**
   * Helper to construct the url
   * @param {*} app 
   * @param {*} endpoint  
   * @param {*} env
   * @param {*} params 
   */
  getUrl: function getUrl(_ref) {
    var app = _ref.app,
        endpoint = _ref.endpoint,
        env = _ref.env,
        params = _ref.params;
    var url = this.trim(this.isValidHttpUrl(app) ? app : auth[this.environment(env)]); //if app is a valid URL then this is not needed

    if (apps[app]) {
      url += "/".concat(this.trim(apps[app]));
    } //if app is a valid URL the endpoint is not needed


    if (endpoint) {
      url += "/".concat(this.trim(endpoint));
    }

    url = addQueryParams(params, url);
    return url;
  }
};