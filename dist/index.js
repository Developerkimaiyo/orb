"use strict";

var modules = ["api", "application", "messaging", "order"];

function loadModules() {
  var exports = {};
  modules.forEach(function (module) {
    exports[module] = require("./modules/".concat(module, "/main"));
  });
  return exports;
}

module.exports = loadModules();