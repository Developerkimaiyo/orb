"use strict";

module.exports = {
  webPlatform: {
    urls: {
      staging: "https://webapptest.sendyit.com/",
      production: "https://app.sendyit.com/",
      beta: "https://beta.sendyit.com/"
    }
  },
  adonisPublicApi: {
    urls: {
      staging: "https://apitest.sendyit.com/v2/",
      production: "https://api.sendyit.com/v2/"
    }
  }
};