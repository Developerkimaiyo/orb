"use strict";

module.exports = {
  /**
   * HTTP verbs that are supported by sendy orb's communication module
   */
  http_methods: ["GET", "POST", "PATCH", "UPDATE", "DELETE"],
  auth: {
    staging: "https://authtest.sendyit.com",
    production: "https://auth.sendyit.com"
  },

  /**
   * A list of apps that are currently pre-configured within sendy orb comm's module
   * The key represents apps and value represents paths, relative to auth URLs
   */
  apps: {
    /** Pricing service */
    pricing: "pricing",

    /** Localisation service */
    localisation: "localisation",

    /** Adonis private api */
    adonis_private_api: "adonis",

    /** Adonis public api */
    adonisPublicApi: "",

    /** Orders app */
    orders: "orders",

    /** Dispatch app */
    dispatch: "dispatch",

    /** Time predictions app */
    time: "time"
  }
};