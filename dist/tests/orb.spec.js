"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _orb = _interopRequireDefault(require("../configs/orb"));

var expect = require('chai').expect;

describe('Orb', function () {
  it('should throw an error if initialised with a non existent module', function () {
    var modules = Object.keys(_orb["default"].modules); // expect(() => new Siwa).to.throw(errors.NO_CONF);
  }); // it ('should throw an error if configuration is not an object', () => {
  //   expect(() => new Siwa('conf')).to.throw(errors.INVALID_CONF);
  // });
  // it ('should throw an error if required fields dont exist', () => {
  //   expect(() => new Siwa({})).to.throw(errors.MISSING_FIELDS);
  // });
});