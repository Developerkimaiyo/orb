const configs = require("../../configs/application")

module.exports = app => {
  return configs[app]
}