const api= require("../../api/main")

/**
 * These functions are useful when a user is placing a price request
 */
module.exports = {
  viewport: async ({locations, googleKey}) => {
    return Promise.all(locations.map(async point => {
      const googleUrl = "https://maps.googleapis.com/maps/api/geocode/json"
      const coordinates = point.lat + "," + point.long
      const { results }  = await api.send({ 
        "method": "get",
        "app": googleUrl, 
        "params": {
          "key": googleKey,
          "fields": "geometry",
          "latlng": coordinates
        }
      })
      return {
        coordinates,
        'type': point.type,
        'name': point.name,
        'more': {
          viewport: results.length == 0 ? results : results[0].geometry.viewport 
        }
      }
    }))
  },
}