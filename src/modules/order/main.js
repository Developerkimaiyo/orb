const orderCompletionFunctions = require("./collections/order_completion_functions")
const pricing_functions = require("./collections/pricing_functions")

module.exports = {
  ...orderCompletionFunctions,
  ...pricing_functions,

  /** 
   * Returns the order status given an order
   */
  status: function (order) {
    const status = Number(order.status)
    const orderStatus = Number(order.order_status)
    const confirmStatus = Number(order.confirm_status)
    const deliveryStatus = Number(order.delivery_status)
    if (status === 1) {
      if (orderStatus === 0 && confirmStatus === 0 && deliveryStatus === 0) {
        return 'price request'
      }
      if (orderStatus === 1 && confirmStatus === 0 && deliveryStatus === 0) {
        return 'pending'
      }
      if (orderStatus === 1 && confirmStatus === 1 && deliveryStatus === 0) {
        return 'confirmed'
      }
      if (orderStatus === 1 && confirmStatus === 1 && deliveryStatus === 2) {
        return 'in transit'
      }
      if (orderStatus === 1 && confirmStatus === 1 && deliveryStatus === 3) {
        return 'delivered'
      }
    }
    return 'cancelled';
  },

  /**
   * Checks if an order can be cancelled.
   * @param { object } order
   * @return { object }
   */
  cancellable: function (order, api = 'private') {
    const orderStatus = this.status(order)
    let response = { status: false, message: null }
    if (orderStatus == "pending" || orderStatus == "confirmed") {
      response.status = true
    } else if (orderStatus == "cancelled") {
      response.message = "Order already cancelled"
    } else if (orderStatus == "in transit") {
      const isPrivateApi = api == 'private'
      response.status = isPrivateApi
      response.message = isPrivateApi ? null : "Delivery in progress. Contact customer care to cancel."
    } else {
      response.message = "Could not cancel order. Contact customer care for more details" 
    }
    return response
  }
}