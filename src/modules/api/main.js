const axios = require('axios')
const helpers = require("./helpers")
const validator = require("./validator")

module.exports = {
   /**
   * Sends a communication request to any one of our internal APIs, Apps or services
   * 
   * @param { string } method
   * @param { string } endpoint
   * @param { object } config
   * 
   * @return { promise }
   */
  send: async function (configs) {
    validator.validate(configs)
    const { app, endpoint, ...request } = Object.assign(configs, { 
      url: helpers.getUrl(configs) 
    })
    return new Promise(async (resolve, reject) => {
      try {
        let response = await axios(request)
        resolve(response.data)
      } catch (error) {
        reject({ error, request })
      }
    })
  }
}