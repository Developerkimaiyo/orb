const { apps, auth } = require("../../configs/api")
const customConfigs = require("../../configs/orb")

/**
 * Adds auth key (SRE)
 * @param {*} params 
 * @param {*} url 
 */
function addAuthKey (params) {
  params = params || {}
  if (!params.authkey) {
    params['authkey'] = customConfigs.authKey
  }
  return params
}

/**
 * Adds query parameters
 * @param {*} params 
 * @param {*} url 
 */
function addQueryParams (params, url) {
  params = addAuthKey(params)
  const queryKeys = Object.keys(params)
  url = !url.includes("?") ? `${url}?` : url
  queryKeys.forEach((key, index) => {
    const value = params[key]
    if (Array.isArray(value)) {
      value.forEach((item, index) => {
        url = index < value.length - 1 ? `${url}${key}[]=${item}` : `${url}&${key}[]=${item}`
      })
    } else {
      url = `${url}${key}=${value}`;
    }
    if (index < queryKeys.length - 1) {
      url = `${url}&`
    }
  })
  return url
}

module.exports = {
  trim: function (string) {
    return string.replace(/^\/|\/$/g, '')
  },

  environment: function (env) {
    return env || customConfigs.environment || 'staging'
  },

  /**
   * Checks if a string is a valid http url 
   */
  isValidHttpUrl: function (string) {
    let url;
    try {
      url = new URL(string);
    } catch (_) {
      return false;  
    }
    return url.protocol === "http:" || url.protocol === "https:";
  },

  /**
   * Helper to construct the url
   * @param {*} app 
   * @param {*} endpoint  
   * @param {*} env
   * @param {*} params 
   */
  getUrl: function ({app, endpoint, env, params}) {
    let url = this.trim(this.isValidHttpUrl(app) ? app : (auth[this.environment(env)]))
    
    //if app is a valid URL then this is not needed
    if (apps[app]) {
      url += `/${this.trim(apps[app])}`
    }

    //if app is a valid URL the endpoint is not needed
    if (endpoint) {
      url += `/${this.trim(endpoint)}`
    }

    url = addQueryParams(params, url)
    return url
  }
}