const modules = [
  "api", "application", "messaging", "order",
]

function loadModules () {
  let exports = {}
  modules.forEach(module => {
    exports[module] = require(`./modules/${module}/main`)
  })
  return exports
}

module.exports = loadModules()