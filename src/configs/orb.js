const fs = require('fs')

const customConfigs = () => {
  const path = require('path').resolve('./config/sendy.js')
  return fs.existsSync(path) ? require(path).orb || {} : {}
}

module.exports = {
  ...customConfigs()
}